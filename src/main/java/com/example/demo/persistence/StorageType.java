/*
 * Algebra labs.
 */

package com.example.demo.persistence;

public enum StorageType {
	IN_MEMORY, CLOUD
}
