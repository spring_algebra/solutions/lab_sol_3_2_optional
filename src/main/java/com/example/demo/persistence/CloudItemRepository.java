/*
 * Algebra labs.
 */
 
package com.example.demo.persistence;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Named;

import com.example.demo.domain.MusicCategory;
import com.example.demo.domain.MusicItem;

// Add @Named annotation
@Named
@RepositoryType(StorageType.CLOUD)   // Add RepositoryType qualifier specifying CLOUD
public class CloudItemRepository implements ItemRepository {
	
	private static MusicItem theItem = new MusicItem(1L,"Title", "artist", "2013-08-01", new BigDecimal("15.99"), MusicCategory.Pop);
	private static ArrayList<MusicItem> theData = new ArrayList<MusicItem>(1);

	{ theData.add(theItem); }

	@Override
	public MusicItem get(Long id) {
		// TODO Auto-generated method stub
		return theItem;
	}

	@Override
	public Collection<MusicItem> getAll() {
			return theData;
	}

	@Override
	public Collection<MusicItem> searchByArtistTitle(String keyword) {
		return theData;
	}

	@Override
	public int size() {
		return theData.size();
	}

	@Override
	public String toString() {
		return "CloudItemRepository";
	}
}
