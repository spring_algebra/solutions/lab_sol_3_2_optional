/*
 * Algebra labs.
 */

package com.example.demo.service;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;

import com.example.demo.domain.MusicItem;
import com.example.demo.persistence.Cloud;
import com.example.demo.persistence.InMemory;
import com.example.demo.persistence.ItemRepository;
import com.example.demo.persistence.RepositoryType;
import com.example.demo.persistence.StorageType;
import com.example.demo.persistence.Version;

@Named
public class CatalogImpl implements Catalog {

	@Inject
	@RepositoryType(StorageType.IN_MEMORY)   // Qualify to use StorageType.IN_MEMORY
	@Version(2.0)  // Add Version Qualifier
	private ItemRepository itemRepository;

	public void setItemRepository(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	public MusicItem findById(Long id) {
		return itemRepository.get(id);
	}

	public Collection<MusicItem> findByKeyword(String keyword) {
		return itemRepository.searchByArtistTitle(keyword);
	}

	@Override
	public int size() {
		return itemRepository.size();
	}
	
	@Override
	public String toString() {
		return "I am a shiny new " + getClass().getName() + " brought to you from Spring" + " but you can just call me " + getClass().getInterfaces()[0] + "\nMy itemRepository is " + itemRepository;
	}

}
